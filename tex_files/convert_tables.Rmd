---
title: TABLES for "Predicting Nuisance Flooding Along the Coasts of United States Using Generalized Linear Models and Gaussian Processes"
output:
  pdf_document: 
    keep_tex: true
  html_document:
    fig_caption: yes
  word_document: default 
---

```{r setup, include=FALSE}
library(knitr)
knitr::opts_chunk$set(echo = TRUE)
lib_res <- file.path("~/Documents/sea_level_final/results/")
```

```{r fig.width=9, echo=FALSE}
library(png)
library(grid)
#img <- readPNG(file.path(lib_res, "single_station.png")
#grid.raster(img)
```

## Mean absolute errors

```{r, results='asis', echo=FALSE}
load(file.path(lib_res, "named_errors_2001_on.dat"))
l1err <- l1err[sort(row.names(l1err)),]
kable(t(l1err)[,1:9], right=FALSE, digits=1)
kable(t(l1err)[,10:18], right=FALSE, digits=1)
```

## Coverage.

```{r, results='asis', echo=FALSE}
load(file.path(lib_res, "coverage_percent_2001_on.dat"))
print(cov_percent$coverage, right=FALSE, digits=3)
```

## Posterior predictive quantiles

```{r, results='asis', echo=FALSE, title='2030 quantiles'}
load(file.path(lib_res, "predictive_quantiles2030.RData"))
preds <- cbind(quants$quants2.6, quants$quants8.5)
colnames(preds) <- c("(RCP2.6) 5%",  "17%", "50%", "83%", "95%", "(RCP8.5) 5%",  "17%", "50%", "83%", "95%")

# Now tack on highest and average NF observed so far
source(file.path(dirname(lib_res), "tex_files/get_station.R"))
f_max <- function (n) max(get_station(n)$nf)
f_mean <- function (n) {
  st <- get_station(n)
  return(round(mean(st$nf[st$years>=2000])))
}

preds <- as.data.frame(preds)
preds$max <- lapply(1:18, f_max)
preds$mean <- lapply(1:18, f_mean)

preds <- preds[sort(rownames(preds)), ]

kable(preds, right=FALSE)
```
