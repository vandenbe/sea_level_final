data {
  int<lower=0> Ntrain;
  int<lower=0> Ntest;
  int<lower=0> N;
  vector[Ntrain] xtrain;
  vector[Ntest] xtest;
  vector[N] x_all;
  int<lower=1, upper=N> train_idx[Ntrain];
  int ytrain[Ntrain];
  simplex[Ntest] pdf1;
  simplex[Ntest] pdf2;
  simplex[Ntest] pdf3;
  int single_predict;
}
transformed data {
  int M;
  real<lower=1> paret;
  paret<-3;
  M <- 365*24; // number of hours in a year == max number of NF days
}
parameters {
  real alpha;
  real betas;
}
model {
  // glm for binomial observations of y given x
  alpha ~ normal(0,10);
  betas ~ normal(0,10);
  ytrain ~ poisson_log(alpha + betas * xtrain); // + y_noise[t]);
}
generated quantities {
  vector[N] y_pred;
  vector[N] y_gen;
  vector[3] y_future;
  vector[Ntrain] log_lik;
  for (t in 1:Ntrain) {
    real lograte;
    lograte <- alpha + betas * xtrain[t];
    log_lik[t] <- poisson_log_log(ytrain[t], lograte);
  }
  y_pred <- exp(alpha + betas * x_all);
  for (t in 1:N) {
    real rate;
    rate <- alpha + betas * x_all[t];
    if (rate > 20) {
      rate <- 20.;
    }
    y_gen[t] <- poisson_log_rng(rate); // posterior predictive samples
  }
  if (single_predict) {
    vector[3] x_future;
    x_future[1] <- xtest[categorical_rng(pdf1)];
    x_future[2] <- xtest[categorical_rng(pdf2)];
    x_future[3] <- xtest[categorical_rng(pdf3)];
    for (tf in 1:3) {
      real rate;
      rate <- alpha + betas * x_future[tf];
      if (rate > 20) {
        rate <- 20.;
      }
      y_future[tf] <- poisson_log_rng(rate);
    }
  } else {
    y_future <- rep_vector(0., 3);
  }
}
