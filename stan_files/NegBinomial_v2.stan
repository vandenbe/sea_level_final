data {
  int<lower=0> Ntrain;
  int<lower=0> Ntest;
  int<lower=0> N;
  vector[Ntrain] xtrain;
  vector[Ntest] xtest;
  vector[N] x_all;
  int<lower=1, upper=N> train_idx[Ntrain];
  int ytrain[Ntrain];
  simplex[Ntest] pdf1;
  simplex[Ntest] pdf2;
  simplex[Ntest] pdf3;
  int single_predict;
}
parameters {
  real<lower=0.> sig_y; // dispersion parameter
  real alpha;
  real beta;
}
model {
  // glm for binomial observations of y given x
  alpha ~ normal(0,100);
  beta ~ normal(0,100);
  sig_y ~ lognormal(0., 1);
  for (t in 1:Ntrain) {
    ytrain[t] ~ neg_binomial_2_log(alpha + beta * xtrain[t], sig_y);
  }
}
generated quantities {
  vector[N] y_pred;
  vector[N] y_gen;
  vector[3] y_future;
  vector[Ntrain] log_lik;
  for (t in 1:Ntrain) {
    real lograte;
    lograte <- alpha + beta * xtrain[t];
    log_lik[t] <- neg_binomial_2_log_log(ytrain[t], lograte, sig_y);
  }
  y_pred <- exp(alpha + beta * x_all);
  for (t in 1:N) {
    real rate;
    rate <- alpha + beta * x_all[t];
    if (rate > 10) {
      rate <- 10; //prevent overflow
    }
    y_gen[t] <- neg_binomial_2_log_rng(rate, sig_y); // posterior predictive samples
  }
  if (single_predict) {
    vector[3] x_future;
    int xindx[3];
    xindx[1] <- categorical_rng(pdf1);
    xindx[2] <- categorical_rng(pdf2);
    xindx[3] <- categorical_rng(pdf3);
    for (tf in 1:3) {
      real rate;
      x_future[tf] <- xtest[xindx[tf]];
      rate <- alpha + beta * x_future[tf];
      if (rate > 10) {
        rate <- 10.;
      }
      y_future[tf] <- neg_binomial_2_log_rng(rate, sig_y);
    }
  } else {
    y_future <- rep_vector(0., 3);
  }
}
