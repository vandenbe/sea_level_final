data {
  int<lower=0> Ntrain;
  int<lower=0> Ntest;
  int<lower=0> N;
  vector[Ntrain] xtrain;
  vector[Ntest] xtest;
  vector[N] x_all;
  int<lower=1, upper=N> train_idx[Ntrain];
  int ytrain[Ntrain];
  simplex[Ntest] pdf1;
  simplex[Ntest] pdf2;
  simplex[Ntest] pdf3;
  int single_predict;
}
transformed data {
  real lengthscale;
  int M;
  matrix[N,N] sq_dist; // matrix of distances |z^k - z^l|
  // Gram matrix of GP kernel evaluated at centroids of regions
  matrix[N,N] K; 
  matrix[N,N] mins;
  matrix[N, N] Kchol; // cholesky factor of K
  lengthscale <- 0.2;
  M <- 365*24; // number of hours in a year == max number of NF days
  for (k in 1:N) {
    for (l in k:N) {
      mins[k,l] <- fmin(x_all[k], x_all[l]);
      mins[l,k] <- mins[k,l];
      sq_dist[k, l] <- (x_all[k] - x_all[l])^2; // z' * z 
      sq_dist[l, k] <- sq_dist[k, l];
      K[k,l] <- .5 * sqrt(sq_dist[k,l]) * mins[k,l]^2 + (mins[k,l]^3)/3.;
      K[l,k] <- K[k,l];
    }
  }
  // only compute upper triangle -- helps if we want to sample over kernel parameters later.
  //K <- exp(-sq_dist / pow(lengthscale, 2));
  // add jitter to diagonal elements:
  for (k in 1:N){
    K[k,k] <- K[k,k] + 0.001;  // Note: sq_dist[k,k] = 1.
  }
  Kchol <- cholesky_decompose(K);
  
}
parameters {
  //real<lower=0.> sig_disper; // dispersion parameter
  vector[N] log_disper;
  real a;
  real b;
  real<lower=0> tau;
  real alpha;
  real beta;
}
transformed parameters {
  vector[N] disper;
  disper <- exp(a + b * x_all + tau * log_disper);
}
model {
  // glm for binomial observations of y given x
  //Knew <- K + diag_matrix(rep_vector(1., N)) * sig2;
  alpha ~ normal(0, 100);
  beta ~ normal(0, 100);
  tau ~ lognormal(0, 1);

  log_disper ~ multi_normal_cholesky(rep_vector(0., N), Kchol);
  for (t in 1:Ntrain) {
    ytrain[t] ~ neg_binomial_2_log(alpha + beta * xtrain[t], disper[train_idx[t]]);
  }
}
generated quantities {
  vector[N] y_pred;
  vector[N] y_gen;
  vector[3] y_future;
  int xindx[3];
  vector[3] x_future;
  vector[Ntrain] log_lik;
  for (t in 1:Ntrain) {
    real lograte;
    lograte <- alpha + beta * xtrain[t];
    log_lik[t] <- neg_binomial_2_log_log(ytrain[t], lograte, disper[train_idx[t]]);
  }
  xindx[1] <- categorical_rng(pdf1);
  xindx[2] <- categorical_rng(pdf2);
  xindx[3] <- categorical_rng(pdf3);
  x_future[1] <- xtest[xindx[1]];
  x_future[2] <- xtest[xindx[2]];
  x_future[3] <- xtest[xindx[3]];
  
  y_pred <- exp(alpha + beta * x_all);
  for (t in 1:N) {
    real rate;
    real dis;
    rate <- alpha + beta * x_all[t];
    if (rate > 12) {
      rate <- 12.; //prevent overflow
    }
    if (disper[t] < 0.0001) { 
        dis <- 0.0001;
    } else {
      dis <- disper[t];
    }
    
    y_gen[t] <- neg_binomial_2_log_rng(rate, dis); // posterior predictive samples
  }
  if (single_predict) {
    for (tf in 1:3) {
      real rate;
      real dis;
      dis <- disper[Ntrain+xindx[tf]];
      rate <- alpha + beta * x_future[tf];
      if (rate > 12) {
        rate <- 12.;
      }
      if (dis < 0.0001) {
        dis <- 0.0001;
      }
      y_future[tf] <- neg_binomial_2_log_rng(rate, dis);
    }
  } else {
    y_future <- rep_vector(0., 3);
  }
}
